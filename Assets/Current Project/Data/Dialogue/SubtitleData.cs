using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Subtitle Data", menuName = "Dementia/Subtitle Data", order = 1)]
public class SubtitleData : ScriptableObject
{
    [TextArea] public List<string> chat;
}