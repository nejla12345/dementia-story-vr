using UnityEngine;
using RedBlueGames.Tools.TextTyper;
using Sirenix.OdinInspector;
using TMPro;

public class SubtitleManager : MonoBehaviour
{
    public SubtitleData data;
    public TextTyper typer;
    public int chatIndex;

    [Button] public void TypeSubtitleContinuously()
    {
        if(chatIndex < data.chat.Count)
        {
            typer.TypeText(data.chat[chatIndex]);
            chatIndex++;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void TypeSubtitleSpecific(int chatIndex)
    {
        typer.GetComponent<TextMeshProUGUI>().text = "";
        typer.TypeText(data.chat[chatIndex]);
    }
}
