using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using DG.Tweening;
using HurricaneVR.Framework.Shared;

public class FollowCameraUI : MonoBehaviour
{
    [ShowInInspector] public bool followPlayer;
    public float followingDistance = 0.5f;
    private Transform canvasCallTargetPos;
    private Coroutine callCoroutine;
    private Camera mainCamera;
    private bool onCamera;

    private float callPressButtonTime;
    private bool callPressing;

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        Vector3 viewPos = mainCamera.WorldToViewportPoint(transform.position);
        if (viewPos.x >= 0.2f && viewPos.x <= 0.8f && viewPos.y >= 0.2f && viewPos.y <= 0.8f && viewPos.z > 0)
        {
            // Your object is in the range of the camera, you can apply your behaviour
            onCamera = true;
        }
        else
            onCamera = false;

        if(!onCamera && followPlayer)
        {
            CallCanvas();
        }

        UseControllerToCallCanvas();
    }

    public void UseControllerToCallCanvas()
    {
        if (callPressing)
            callPressButtonTime += Time.deltaTime;

        var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

        if (buttonState.JustActivated)
            callPressing = true;

        if (buttonState.JustDeactivated)
        {
            callPressing = false;
            if (callPressButtonTime <= 1f)
            {
                CallCanvas();
            }

            callPressButtonTime = 0;
        }

        if (callPressButtonTime > 1f)
        {
            //igmPanel.gameObject.SetActive(true);
        }
    }

    [Button] public void CallCanvas()
    {
        if (callCoroutine != null) return;
        StartCoroutine(CallCanvasCoroutine(transform));
    }

    private IEnumerator CallCanvasCoroutine(Transform canvas)
    {
        if (canvas)
        {
            Sequence seq = DOTween.Sequence();
            seq.Append(canvas.DOMove(mainCamera.transform.position + (mainCamera.transform.forward * followingDistance), 0.4f));
            seq.Append(canvas.DOLookAt(mainCamera.transform.position + (mainCamera.transform.forward * followingDistance * 2), 0.4f, AxisConstraint.Y));
            yield return new WaitForSeconds(0.4f);
        }
    }
}