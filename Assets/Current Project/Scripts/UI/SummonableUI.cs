using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using HurricaneVR.Framework.Shared;

public class SummonableUI : MonoBehaviour
{
    [Header("Main")]
    [FoldoutGroup("Main"), SerializeField] private Rigidbody canvasMain;
    [FoldoutGroup("Main"), SerializeField] private Transform canvasCallTargetPos;
    private float callPressButtonTime;
    private bool callPressing;
    private Coroutine callCoroutine;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        CallCanvas();

        if (callPressing)
            callPressButtonTime += Time.deltaTime;

        var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

        if (buttonState.JustActivated)
            callPressing = true;

        if (buttonState.JustDeactivated)
        {
            callPressing = false;
            if (callPressButtonTime <= 1f)
            {
                if (callCoroutine != null) StopCoroutine(callCoroutine);
                callCoroutine = StartCoroutine(CallCanvasCoroutine());
            }

            callPressButtonTime = 0;
        }
    }

    private void Update()
    {
        CallCanvas();

        if (callPressing)
            callPressButtonTime += Time.deltaTime;

        var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

        if (buttonState.JustActivated)
            callPressing = true;

        if (buttonState.JustDeactivated)
        {
            callPressing = false;
            if (callPressButtonTime <= 1f)
            {
                if (callCoroutine != null) StopCoroutine(callCoroutine);
                callCoroutine = StartCoroutine(CallCanvasCoroutine());
            }

            callPressButtonTime = 0;
        }
    }

    public void CallCanvas()
    {
        if (callPressing)
            callPressButtonTime += Time.deltaTime;

        var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

        if (buttonState.JustActivated)
        {
            callPressing = true;
        }

        if (buttonState.JustDeactivated)
        {
            callPressing = false;
            if (callPressButtonTime <= 1f)
            {
                if (callCoroutine != null) StopCoroutine(callCoroutine);
                callCoroutine = StartCoroutine(CallCanvasCoroutine());
            }

            callPressButtonTime = 0;
        }
    }

    [Button] public void ForceCallCanvas()
    {
        if (callCoroutine != null) StopCoroutine(callCoroutine);
        callCoroutine = StartCoroutine(CallCanvasCoroutine());
    }

    private IEnumerator CallCanvasCoroutine()
    {
        if (canvasMain)
        {
            canvasMain.detectCollisions = false;
            Sequence seq = DOTween.Sequence();
            seq.Append(canvasMain.transform.DOMove(canvasCallTargetPos.position, 0.2f));
            seq.Append(canvasMain.transform.DOLookAt(Camera.main.transform.position, 0.2f, AxisConstraint.Y));
            yield return new WaitForSeconds(0.4f);
            canvasMain.detectCollisions = true;
        }
    }
}
