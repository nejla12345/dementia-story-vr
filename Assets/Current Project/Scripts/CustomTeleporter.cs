using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HurricaneVR.Framework.Core.Player;
using Sirenix.OdinInspector;

public class CustomTeleporter : MonoBehaviour
{
    public HVRTeleporter teleporter;
    public Transform destination;
    public HVRCanvasFade canvasFade;

    [Button] 
    public void TeleportInstant()
    {
        teleporter.Teleport(destination.position, destination.forward, true);
    }
    [Button]
    public void TeleportWithFade()
    {
        canvasFade.FadeEnd.AddListener(delegate 
        {
            teleporter.Teleport(destination.position, destination.forward, true);
            canvasFade.Fade(0, 1);
            canvasFade.FadeEnd.RemoveAllListeners();
        });
        canvasFade.Fade(1, 1);
    }
}
