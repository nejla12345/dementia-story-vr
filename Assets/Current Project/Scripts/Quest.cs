using Sirenix.OdinInspector;
using System;
using UnityEngine;
using UnityEngine.Events;

public class Quest : MonoBehaviour
{
    [TextArea(1, 10)] public string questName;
    [TextArea(3, 10)] public string questDescription;
    public int progress;
    public int goal;
    public bool completed;
    private bool completedPreviousFrame;
    [FoldoutGroup("Progress Events")] public UnityEvent onProgressChanged;
    [FoldoutGroup("Progress Events")] public UnityEvent onProgressAdded;
    [FoldoutGroup("Progress Events")] public UnityEvent onProgressSubtracted;
    [FoldoutGroup("Progress Events")] public UnityEvent onCompleted;
    [FoldoutGroup("Progress Events")] public UnityEvent onUncompleted;

    private void Update()
    {
        completed = progress >= goal;

        if(completed && !completedPreviousFrame) onCompleted?.Invoke();
        else if(!completed && completedPreviousFrame) onUncompleted?.Invoke();

        completedPreviousFrame = completed;
    }

    public void AddProgress()
    {
        progress++;
        onProgressChanged?.Invoke();
        onProgressAdded?.Invoke();
    }

    public void SubtractProgress()
    {
        progress--;
        onProgressChanged?.Invoke();
        onProgressSubtracted?.Invoke();
    }

    public void ForceCompleteProgress()
    {
        progress = goal;
    }

    public void ForceResetProgress()
    {
        progress = 0;
    }
}
