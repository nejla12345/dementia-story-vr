using UnityEngine;
using HurricaneVR.Framework.Components;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class HVRPhysicsDoorExtended : HVRPhysicsDoor
{
    [FoldoutGroup("Extension")] public UnityEvent onDoorUnlatchedEvent;
    [FoldoutGroup("Extension")] public UnityEvent onDoorLatchedEvent;
    [FoldoutGroup("Extension")] public UnityEvent onDoorClosedEvent;
    [FoldoutGroup("Extension")] public UnityEvent onDoorOpenedEvent;

    protected override void Update()
    {
        Joint.targetAngularVelocity = new Vector3(TargetAngularVelocity, 0f, 0f);

        if (_doorClosing)
            return;

        if (Tracker.UnsignedAngle < CloseAngle)
        {
            _detectionTimer += Time.deltaTime;
        }
        else if (Tracker.UnsignedAngle >= CloseAngle)
        {
            _detectionTimer = 0f;
            DoorClosed = false;
        }

        if (HandleGrabbable && HandleGrabbable.IsBeingHeld || SecondHandleGrabbable && SecondHandleGrabbable.IsBeingHeld)
        {
            _detectionTimer = 0f;
        }

        if (_detectionTimer > CloseDetectionTime)
        {
            DoorClosed = true;
        }

        if (!PreviousClosed && DoorClosed)
        {
            OnDoorClosed();
            onDoorClosedEvent?.Invoke();
        }
        else if (PreviousClosed && !DoorClosed)
        {
            OnDoorOpened();
            onDoorOpenedEvent?.Invoke();
        }

        var reset = SFXResetThreshold;
        if (SFXResetThreshold > SFXThresholdAngle)
        {
            reset = SFXThresholdAngle * .5f;
        }

        if (!Opened && Tracker.UnsignedAngle > SFXThresholdAngle && Time.time - _lastOpenedSFXTime > SFXTimeout)
        {
            _lastOpenedSFXTime = Time.time;
            Opened = true;
            PlayOpenedSFX();
        }
        else if (!Closed && Tracker.UnsignedAngle < SFXThresholdAngle && Time.time - _lastClosedSFXTime > SFXTimeout)
        {
            _lastClosedSFXTime = Time.time;
            Closed = true;
            PlayClosedSFX();
        }
        else if (Opened && Tracker.UnsignedAngle < SFXThresholdAngle - reset)
        {
            Opened = false;
        }
        else if (Closed && Tracker.UnsignedAngle > SFXThresholdAngle + reset)
        {
            Closed = false;
        }

        if (HandleRequiresRotation)
        {
            if (HandleRotationTracker.UnsignedAngle >= HandleThreshold ||
                (SecondHandleRotationTracker && SecondHandleRotationTracker.UnsignedAngle >= HandleThreshold))
            {
                DoorLatched = false;
            }
            else if (HandleRotationTracker.UnsignedAngle < HandleThreshold &&
                     (!SecondHandleRotationTracker || SecondHandleRotationTracker.UnsignedAngle < HandleThreshold) &&
                     Tracker.UnsignedAngle < CloseAngle)
            {
                DoorLatched = true;
            }

            if (!Locked)
            {
                if (PreviousDoorLatched && !DoorLatched)
                {
                    OnDoorUnLatched();
                    onDoorUnlatchedEvent?.Invoke();
                }
                else if (!PreviousDoorLatched && DoorLatched)
                {
                    OnDoorLatched();
                    onDoorLatchedEvent?.Invoke();
                }
            }
        }

        PreviousDoorLatched = DoorLatched;
        PreviousClosed = DoorClosed;
    }
}
