using System;
using UnityEngine;
using HurricaneVR.Framework.Components;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using HurricaneVR.Framework.Core;

public class HVRRotationTrackerExtended : HVRRotationTracker
{
    [FoldoutGroup("Extension")] public float angleCheckpoint;
    [FoldoutGroup("Extension")] public float checkpointTolerance;
    [FoldoutGroup("Extension")] [ShowInInspector] [ReadOnly] private bool previouslyCheckpointMatched;
    [FoldoutGroup("Extension")] [ShowInInspector] [ReadOnly] private bool checkpointMatched;
    [FoldoutGroup("Extension")] public RotationTrackerAngleCheckpointEvent AngleCheckpointMatched = new RotationTrackerAngleCheckpointEvent();
    [FoldoutGroup("Extension")] public RotationTrackerAngleCheckpointEvent AngleCheckpointUnmatched = new RotationTrackerAngleCheckpointEvent();
    protected override void Update()
    {
        var currentVector = transform.localRotation * _trackAxis;

        ClampedAngle = Vector3.SignedAngle(_startVector, currentVector, AxisOfRotation);

        if (ClampedAngle < 0)
        {
            ClampedAngle += 360;
        }

        var angleDelta = Vector3.SignedAngle(_angleVector, currentVector, AxisOfRotation);
        if (Mathf.Abs(angleDelta) > AngleThreshold)
        {
            _angleVector = currentVector;
            Angle += angleDelta;
            OnAngleChanged(ClampedAngle, angleDelta);
        }

        //angle checkpoint
        checkpointTolerance = Mathf.Clamp(checkpointTolerance, 1, checkpointTolerance);
        if ((Mathf.CeilToInt(Angle) <= angleCheckpoint + checkpointTolerance) && (Mathf.CeilToInt(Angle) >= angleCheckpoint - checkpointTolerance))
        {
            checkpointMatched = true;
            if(!previouslyCheckpointMatched) OnAngleCheckpointMatched(ClampedAngle, angleDelta);
        }
        else
        {
            checkpointMatched = false;
            if (previouslyCheckpointMatched) OnAngleCheckpointUnmatched(ClampedAngle, angleDelta);
        }

        previouslyCheckpointMatched = checkpointMatched;
        //end angle checkpoint

        var stepAngle = ClampedAngle;
        if (Steps > 1)
        {
            Step = Mathf.RoundToInt(ClampedAngle / StepSize);
            stepAngle = Step * StepSize;
            if (Mathf.Approximately(stepAngle, 360))
            {
                Step = 0;
            }
        }

        if (Steps > 1)
        {
            if (!Mathf.Approximately(stepAngle, _previousAngle))
            {
                OnStepChanged(Step, true);
                _previousAngle = stepAngle;
            }
        }

        _angleFromStart = Angle;
        _clampedAngle = ClampedAngle;
    }

    protected virtual void OnAngleCheckpointMatched(float angle, float delta)
    {
        AngleCheckpointMatched.Invoke(angle, delta);
        //GetComponent<HVRGrabbable>().ForceRelease();
    }
    protected virtual void OnAngleCheckpointUnmatched(float angle, float delta)
    {
        AngleCheckpointUnmatched.Invoke(angle, delta);
    }
}

[Serializable]
public class RotationTrackerAngleCheckpointEvent : UnityEvent<float, float> { }
