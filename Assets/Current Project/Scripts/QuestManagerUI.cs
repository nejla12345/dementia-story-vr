using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class QuestManagerUI : MonoBehaviour
{
    public QuestManager questManager;
    public GameObject questListItemPrefab;
    public Transform questListItemParent;
    [ShowInInspector][ReadOnly][SerializeField] private List<GameObject> questList = new List<GameObject>();



    // Start is called before the first frame update
    void Start()
    {
        InitializeQuestManagerUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitializeQuestManagerUI()
    {
        List<Quest> quests = questManager.quests;
        for (int questsCount = 0; questsCount < quests.Count; questsCount++)
        {
            var questListItem = Instantiate(questListItemPrefab, questListItemParent);
            QuestListItemUI ui = questListItem.GetComponent<QuestListItemUI>();
            ui.questName.text = quests[questsCount].questName;
            ui.questDescription.text = quests[questsCount].questDescription;
            ui.questProgress.text = quests[questsCount].progress + " / " + quests[questsCount].goal;

            questListItem.name = "UI " + quests[questsCount].name; 
            questList.Add(questListItem);
        }

        for (int questsCount = 0; questsCount < quests.Count; questsCount++)
        {
            int index = questsCount;
            quests[index].onProgressChanged.AddListener(delegate
            {
                UpdateQuestManagerUI(index);
            });
        }
    }

    public void UpdateQuestManagerUI(int questIndex)
    {
        QuestListItemUI item = questList[questIndex].GetComponent<QuestListItemUI>();
        item.questName.text = questManager.quests[questIndex].questName;
        item.questDescription.text = questManager.quests[questIndex].questDescription;
        item.questProgress.text = questManager.quests[questIndex].progress + " / " + questManager.quests[questIndex].goal;
    }
}
