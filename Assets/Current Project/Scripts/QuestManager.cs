using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class QuestManager : MonoBehaviour
{
    public List<Quest> quests;
    [Space]
    public UnityEvent onAllQuestsCompleted;
    public UnityEvent onUndoAllQuestsCompleted;
    [FoldoutGroup("Debug")] [ShowInInspector] [ReadOnly] public int allQuestsProgress { get; private set; }
    [FoldoutGroup("Debug")] [ShowInInspector] [ReadOnly] public int allQuestsGoal { get; private set; }
    [FoldoutGroup("Debug")] [ShowInInspector] [ReadOnly] public bool allQuestsCompleted { get; private set; }
    private bool allQuestsCompletedPreviousFrame = false;

    private void Start()
    {
        Initialize();
    }

    private void Update()
    {
        allQuestsCompleted = (allQuestsProgress == allQuestsGoal);

        if (allQuestsCompleted && !allQuestsCompletedPreviousFrame)
            onAllQuestsCompleted?.Invoke();
        else if (!allQuestsCompleted && allQuestsCompletedPreviousFrame)
            onUndoAllQuestsCompleted?.Invoke();

        allQuestsCompletedPreviousFrame = allQuestsCompleted;
    }
    public void Initialize()
    {
        allQuestsGoal = GetTotalQuestsGoal();

        for (int questsCount = 0; questsCount < quests.Count; questsCount++)
        {
            int index = questsCount;
            quests[index].onCompleted.AddListener(delegate
            {
                allQuestsProgress++;
            });
            quests[index].onUncompleted.AddListener(delegate
            {
                allQuestsProgress--;
            });
        }
    }

    public int GetTotalQuestsGoal()
    {
        return quests.Count;
    }
}
