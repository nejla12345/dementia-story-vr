using System;
using Sirenix.OdinInspector;
using TMPro;

public class QuestListItemUI : SerializedMonoBehaviour
{
    public TextMeshProUGUI questName;
    public TextMeshProUGUI questDescription;
    public TextMeshProUGUI questProgress;
}
